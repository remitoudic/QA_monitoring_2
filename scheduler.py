import schedule
import time
import import_process_local_machine as job_to_do
#https://media.readthedocs.org/pdf/schedule/stable/schedule.pdf
import schedule
import time, os
import import_process_from_qa_machine as job_to_do_qamachine
import import_process_local_machine as job_to_do_for_local_machine
#https://media.readthedocs.org/pdf/schedule/stable/schedule.pdf

hostname = os.popen("hostname").read()

print( '###  QA Project on' ,hostname )
def job():
    print("Processing...")
    if 'qa' in hostname:
        job_to_do_qamachine.main()
    else:
        job_to_do_for_local_machine.main()
    print(" working...")


if 'qa' in hostname:
    schedule.every(1).minutes.do(job)
else:
    schedule.every(1).minutes.do(job)


#schedule.every(10).seconds.do(job)
#schedule.every().hour.do(job)
#schedule.every().day.at("10:30").do(job)
#schedule.every().monday.do(job)
#schedule.every().wednesday.at("13:15").do(job)
while True:
    schedule.run_pending()
