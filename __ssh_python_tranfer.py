from subprocess import  PIPE , Popen
import ast, pandas, re
#
#   here  we make pull data from the   qa machine through pipe and ssh
# the  "__get_data_QA_machine.py" file must be in the same directory.
#  transfer_data() need as arguemnt a name of a qa machine and in the  pull_data_from_QA_machine ()
#
def pull_data_from_QA_machine( name_QA_machine):
    ' !!!  output = pandas dataframe'
    p1 = open("__get_data_QA_machine.py",'r')
    p2 = Popen(["ssh",'-oStrictHostKeyChecking=no' , name_QA_machine, "python3"], stdin=p1, stdout=PIPE)
    # '-oStrictHostKeyChecking=no'  # to avoid  : 'Are you sure you want to continue connecting (yes/no)? Host key verification failed.'


    output = p2.communicate()
    #print('raw data', output)
    #p2.kill()
    if  len(output[0])> 10:  # because no byte  transfer come out with : b'[{}]\n
        #print(output[0])
        output_python_data= ast.literal_eval(output[0].decode("utf-8"))
        if isinstance( pandas.DataFrame(output_python_data), pandas.DataFrame): # if empty dataframe
            return (name_QA_machine, pandas.DataFrame(output_python_data))
        else:
             return [ name_QA_machine, [] ]
    else:
        return [ name_QA_machine, [] ]


def filter_issue_key(DF_from_pull_data_from_QA_machine):
    ' !!!!   output= Dataframe'
    DF=DF_from_pull_data_from_QA_machine[1]
    name_QA_machine=DF_from_pull_data_from_QA_machine[0]

    print(DF)
    if isinstance(DF, pandas.DataFrame):
        filter_issue_key=[]
        for elt in list(DF['branch']):
            #print (elt)
            if re.search('BOSS-([0-9]*)', elt):
                filter_issue_key.append(re.findall('BOSS-[0-9]*', elt)[0])
            elif elt == 'master':
                filter_issue_key.append('master')
            else:
                filter_issue_key.append(' not in  BOSS and not master')
        DF['issues_key']= filter_issue_key
        return (name_QA_machine,DF)
    else:
         return(name_QA_machine,[])


def transfer_data (name_QA_machine):
    # equivalent to  the bash command : cat ssh_get_data_QA_machine.py | ssh qa0668 'python'
    data=pull_data_from_QA_machine(name_QA_machine)
    post_processing= filter_issue_key(data)[1]
    name_QA_machine=filter_issue_key(data)[0]
    return(post_processing)

#print('result',transfer_data('qa0670'))

# https://stackoverflow.com/questions/51424025/python-replacing-shell-pipeline-with-an-ssh-connection/51424364#51424364



''''
an alternative to jump :

from jumpssh import SSHSession

gateway_session = SSHSession('jump-qa.contorion.net','remi.toudic', password='').open()
remote_session = gateway_session.get_remote_session('10.135.38.24', password='')

print (remote_session.get_cmd_output('ls'))

example of raw data :

Date: Fri Jul 13 08:42:25 UTC 2018
Path: story/BOSS-11915-authentication
Revision: 909754d9cc8fb05178fc6caf1e4f20ca84b27c0a
Deployed by: sabrina.fichtner

'''
