import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','QA_monitoring.settings')

# for django
import django
import pandas as pd
django.setup()

# for data processing
from QA_interface_app.models import QA_machine_DB, Report
import __ssh_python_tranfer as QA_machine
import __get_jira_issues as jira_data
import pandas as pd

# for salt
import re, schedule,  time

###
# The class  QA_machine_ store all the data of the a qa machine
#  1) Name (ex:qa1212 , intern ip adress (ex: 10.135.XX.XX)
#  2) #  report correspond to  the information collected by the script of david
#  3) status ( ex: free of busy)

# The class import_process is responsible for :
#  1) find the actual names of the Qa machines working  via salt,
#  2) create for each name a QA_machine_ object in which all the data of the machine is stored
#  3) save in each QA_machine_ object in the databank for Django
#
###

class QA_machine_:

    def __init__(self):
        self.name=None
        self.ip_intern_adress=None
        self.report= None
        self.status= None
        self.qa_machine_validation = False

    def fill_data(self):
        self.report= QA_machine.transfer_data(self.ip_intern_adress)

    def validation(self):
        # check that there is some data.  information are collected in a Dataframe
        if isinstance(self.report,   pd.DataFrame):
            self.qa_machine_validation= True

    def find_state_QA_nmachine( self):
    # get status of the QA machine

        QA = self.report  # type : pd.dataframe
        JIRA= jira_data.DATA # type : pd.dataframe
        JOIN_QA_JIRA = pd.merge(QA.tail(), JIRA, left_on='issues_key', right_on='issues_key').empty
        # to find if intersection between QA and JIRA

        #####################  Logic to find if  QA machine is available
        print( ' ########## DEBUG ###########')
        for line in  range(QA.shape[0]):

            if  not (re.search('BOSS-([0-9]*)', QA.branch[line])):
                if QA.branch[0]== 'master' :    # true =>  the join is empty
                    self.status= 'free'

                else :
                    self.status= 'busy'
                    break
            else:
                if JOIN_QA_JIRA:   #  ask  if JOIN_QA_JIRA empty is (l50)
                    self.status= 'free'
                else:
                    self.status= 'busy'
                    break


    def import_data_django(self):
    #  https://stackoverflow.com/questions/31418714/update-or-create-wont-respect-unique-key
    #  much better exaplain as in the  Django docu

        remote_data= self.report.to_dict('index')
        #print(remote_data )
        #print('##############################',self.name)
        current_machine, _ = QA_machine_DB.objects.update_or_create( QAmachine =self.name,
                            defaults={'status': self.status})

        print (self.name, ' machine creation/update  passed  ! :')

        for key in remote_data.keys():

            Report.objects.update_or_create(service= remote_data[key]['service_name'],
                                                 user=remote_data[key]['user'],
                                                 store= remote_data[key]['shop'],
                                                 deployement_date=remote_data[key]['date_release'],
                                                 dump_date=remote_data[key]['dump_date'],
                                                 branch = remote_data[key]['branch'],
                                                 FK_report= current_machine)
        print ( self.name, '-',self.status, 'in DB :)' , )

class import_process:

    def __init__(self):
        self.List_current_machine=None # data from salt
        self.machines_not_validated=[]

    def clean_report_table(self):
         Report.objects.all().delete()

    def get_actual_machine_with_salt(self):
        # http://docs.saltstack.com/en/pdf/Salt-2018.3.0.pdf  page : 349 - 350
        #  just one minion is used  to get acess to data

	 # process = Popen(['ssh', 'qa0668', 'sudo', 'salt-call mine.get' ,'*' ,'qa.ip_addr',' --out=string '], stdout=PIPE,  stderr= PIPE) # use a qa machine to get the mine , not ask the master

	# similar to bash command : os.popen("sudo salt-call mine.get '*' 'qa.ip_addr' ").read().split('\n')
        # stdout = process.communicate()[0]

        #result= stdout.decode("utf-8").split('\n')
        result = os.popen("sudo salt-call mine.get '*' 'qa.ip_addr' ").read().split('\n')
        list_machine= []
        for elt in result:
            if 'qa' in elt:
                sublist_=[]
                sublist_.append(re.findall('qa[0-9]*', elt)[0])
            elif  '10.135' in elt:
                sublist_.append(elt.replace(' ',''))
                list_machine.append(sublist_)
        self.List_current_machine=list_machine


    def  process_queue(self):
        for i in range(len(self.List_current_machine)) :
            machine= QA_machine_()
            machine.name= self.List_current_machine[i][0]
            machine.ip_intern_adress=self.List_current_machine[i][1]
            print ( machine.name, machine.ip_intern_adress)
            machine.fill_data()
            #print(machine.report)
            machine.validation()
            if machine.qa_machine_validation == True :
                machine.find_state_QA_nmachine()
                machine.import_data_django()
            else:
                print ('################### No Data  for' ,machine.name,machine.ip_intern_adress , '###############' )


#if __name__ == '__main__':
def main():
    #import process
    my_process= import_process()
    my_process.clean_report_table()
    my_process.get_actual_machine_with_salt()
    print ( '#######################  QA machines queue to process : ####################################### ' )
    print( my_process.List_current_machine)
    print ( '#######################  QA machines queue is processing ####################################### ' )
    my_process.process_queue()
    print(' ###### debug queue processing ')
    #print ( '#######################   Process Report  ####################################### ' )
    #print ("the number of qa machine processed is : " , len( [item for sublist in my_process.Queue for item in sublist]))
    #print ('the number of qa machine with no data is : ' , len( my_process.machines_not_validated) )
    print( '################### ITERATION FINISHED ################# ')
