import zipfile, os,  re, datetime
#  For localusage : script that is piped through jumps_host and then run into the QA machine with python 
# can be runned  also with the  command : cat ssh_get_data_QA.py | ssh con-qa-1010 'python'

def data_yves_zed (shop):
    'output =  dict'
    if  os.path.isdir('/data/shop/quality/current')== True :

            os.chdir('/data/shop/quality/current')
            textfile = open('rev.txt', 'r').read()
            user= re.findall('Deployed by: (.*)', textfile) [0]
            branch= re.findall('Path: ([^ ]*)\n', textfile)[0]
            date=re.findall('Date: (.*)\n', textfile)[0]
            # to python date datatype and then with some formating
            date_release= datetime.datetime.strptime(date, '%a %b %d %H:%M:%S %Z %Y').strftime("%d/%m/%y - %H:%M")
            
            date_last_dump= os.popen("unzip -l /data/shop/quality/current/data/"+shop+"/dumps/database_dump.zip").read()
            date_last_dump=re.findall(r"(\d+-\d+-\d+\d+-\d+-\d+-\d+).sql", date_last_dump)
            date_last_dump= datetime.datetime.strptime(date_last_dump[0], '%Y-%m-%d-%H-%M-%S').strftime("%d/%m/%y - %H:%M")

            result= dict()
            result['user']= user
            result['branch']=branch
            result['date_release']=  date_release
            result ['dump_date']= date_last_dump
            result['service_name']='yves & zed'
            result['shop']= shop
    else:
        result= dict()
    return result



def data_others_system():
    data_box = []
    system= ['timmy' , 'quentin','ulf','xerxes','patrick',]

    for service in system :

            service_dict= dict()
            service_dict['service_name'] = service
            path=  '/data/'+service+'/quality/current/release.yml'
            
            if  os.path.isfile(path)== True:
                    path=  '/data/'+service+'/quality/current/release.yml'
                    content=open(path).read()
                    service_dict['shop']= 'XX'
                    service_dict['user'] = re.findall('user: (.*)', content)[0]
                    service_dict['branch']= re.findall('target: (.*)', content)[0]
                    date = os.path.getctime('/data/'+service+'/quality/current/release.yml')
                    service_dict['date_release']= datetime.datetime.fromtimestamp(date).strftime("%d/%m/%y - %H:%M")

              
                    
                    lastdump_path=('/data/'+service+'/quality/current/data/dump/')
                 
                    if os.path.exists(lastdump_path)== True:
                        lastdump_data= os.popen('stat '+lastdump_path+service +'_dump_stripped.sql.gz').read()
                        date_last_dump= re.findall(r"(\Modify+:+\s\d+-\d+-\d+\d+\s\d+:\d+:\d+)", lastdump_data)[0].split('odify: ' )[1]
                        date_last_dump=datetime.datetime.strptime(date_last_dump, '%Y-%m-%d %H:%M:%S').strftime("%d/%m/%y - %H:%M")
                        service_dict['dump_date']= date_last_dump
                    else:
                         service_dict['dump_date']= '-'
                        
                    data_box.append(service_dict)
            

    
    return data_box


def data_QA_machine():

    result= data_others_system()
    if len(data_yves_zed('AT'))!=0  : result.append( data_yves_zed('AT'))
    if len(data_yves_zed('DE'))!=0  :result.append( data_yves_zed('DE'))
    
    print(result) # very important ...
    return result

if __name__ == "__main__":
    result= data_QA_machine()


 # ssh -A remi.toudic@jump-qa.contorion.net
