from django.contrib.auth.models import User
from django.db import models

class UserProfileInfo(models.Model):
    user = models.OneToOneField(User)
    # addditionals
    codewars_username= models.URLField(blank=True)
    profile_pic=models.ImageField(upload_to='profile_pics', blank=True)
    def __str__(self):
        return self.user.name



class  QA_machine_DB(models.Model):
    QAmachine = models.CharField( max_length = 64, unique=True)
    status = models.CharField(max_length=32,null=True, blank=True)
    owner= models.CharField(max_length=32,null=True, blank=True)
    comments= models.CharField(max_length=32,null=True, blank=True)
    def __str__(self):
        return "%s" %  (self.id)

class Report(models.Model):
    # primary key set automatically from Django
    store = models.CharField(max_length= 128,null=True, blank=True)
    service = models.CharField(max_length= 128,null=True, blank=True)
    user = models.CharField(max_length= 64,null=True, blank=True)
    deployement_date = models.CharField(max_length= 128,null=True, blank=True)
    dump_date = models.CharField(max_length= 128,null=True, blank=True)
    branch= models.CharField(max_length= 128, null=True, blank=True)
    #FK_report=models.ForeignKey(QA_machine_DB, on_delete=models.CASCADE)
    FK_report=models.ForeignKey(QA_machine_DB, null=True, blank=True, on_delete=models.SET_NULL)
    #  publications = models.ManyToManyField(Publication)
    def __str__(self):
        return "%s %s" % (self.store, self.user)



## how to reset migrations
## if problem with migrations
#https://stackoverflow.com/questions/34720511/unable-to-make-migrations-after-changes-in-table-in-django-1-8
