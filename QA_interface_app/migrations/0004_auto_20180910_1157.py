# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-10 11:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('QA_interface_app', '0003_auto_20180910_0930'),
    ]

    operations = [
        migrations.AddField(
            model_name='qa_machine_db',
            name='comments',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AddField(
            model_name='qa_machine_db',
            name='owner',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
    ]
