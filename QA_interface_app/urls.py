from django.conf.urls import url
from QA_interface_app import views

# template urls

app_name= 'QA_interface_app'

urlpatterns= [
    url(r'^$', views.report ,name = 'reports'),
    url(r'^register$', views.register ,name = 'register'),
    url(r'^qa_machine_api$', views.qa_machine_API.as_view()),
    url(r'^api/$', views.CreateView.as_view(), name="CreateView"),
    url(r'^user_login$', views.user_login ,name = 'user_login'),
    url(r'^logout/', views.user_logout, name= 'logout'),
    url(r'^readme/', views.readme, name= 'readme'),
    ]
