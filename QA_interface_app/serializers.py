from rest_framework import serializers
from .models import QA_machine_DB
# https://medium.com/@_christopher/basic-web-app-with-django-rest-framework-and-jquery-ajax-c3a6a2ce3188
class qa_machine_serializer(serializers.ModelSerializer):
    class Meta:
        model = QA_machine_DB
        fields = '__all__'
