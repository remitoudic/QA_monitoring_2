from django.contrib import admin
from QA_interface_app.models import Report, QA_machine_DB, UserProfileInfo
# Register your models here.
admin.site.register(Report)
admin.site.register(QA_machine_DB)
admin.site.register(UserProfileInfo)
