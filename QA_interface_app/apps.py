from django.apps import AppConfig


class QaInterfaceAppConfig(AppConfig):
    name = 'QA_interface_app'
