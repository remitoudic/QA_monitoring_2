from django import forms
from django.views.generic.edit import UpdateView
from QA_interface_app.models import Report, QA_machine_DB, UserProfileInfo
from django.contrib.auth.models import User
from django.forms import ModelForm


class UserForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model=User
        fields=['username', 'email', 'password']

class UserProfileInfoForm(forms.ModelForm):
    class Meta():
        model= UserProfileInfo
        fields= ['codewars_username','profile_pic']


class FormReport ( forms.Form):
    store = forms.CharField()
    service = forms.CharField()
    user = forms.CharField()
    deployement_date = forms.CharField()
    dump_date = forms.CharField()
    branch= forms.CharField(widget= forms.Textarea)

class QAmachine (forms.Form):
    QAmachine= forms.CharField()
    QAmachine_status=forms.CharField()
    store = forms.CharField()
    service = forms.CharField()
    user = forms.CharField()
    deployement_date = forms.CharField()
    dump_date = forms.CharField()
    branch= forms.CharField()

class BookingForm(forms.ModelForm):
    make_list_of_tuple = lambda list_machine  :  [tuple( [i,i]) for i in list_machine]
    MACHINES= tuple( QA_machine_DB.objects.values_list('QAmachine',flat=True))
    CHOICE_QA_MACHINES= make_list_of_tuple(MACHINES)
    QAmachine= forms.ModelChoiceField(queryset= QA_machine_DB.objects.values_list('QAmachine',flat=True), empty_label="QA machines")

    class Meta():
        model= QA_machine_DB
        fields= ['QAmachine', 'owner', 'comments','status']
        # http://nanvel.name/2014/03/django-change-modelform-widget
        widgets = {
                   'owner':forms.TextInput(attrs={'placeholder':'owner'}),
                   'comments':forms.TextInput(attrs={'placeholder':'comments'}),
                   'status': forms.RadioSelect( choices=[('busy','busy'),('free','free')])}
