from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from QA_interface_app.models import Report, QA_machine_DB, UserProfileInfo
from QA_interface_app import forms
from QA_interface_app.forms import BookingForm,UserForm,UserProfileInfoForm
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import qa_machine_serializer
from rest_framework import generics
# for login / logout / register
from django.contrib.auth import authenticate , login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required


class qa_machine_API(APIView):
    "  acces to the API with the following command :     "
    def get(self, request):
        queryset = QA_machine_DB.objects.order_by('QAmachine')
        serialized = qa_machine_serializer(queryset, many=True)
        return Response(serialized.data)

    def put(self,request,pk):
        queryset = QA_machine_DB.objects.get(pk=id)
        #request.method == 'PUT':
        serializer = qa_machine_serializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def index( request):
    return render( request,'QA_interface_app/index.html')

def readme( request):
    return render( request,'QA_interface_app/documentation.html')


def register(request):
    registered=False
    if request.method == "POST":
        user_form = UserForm(data=request.POST)
        profile_form= UserProfileInfoForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user= user_form.save()
            user.set_password(user.password)
            user.save()

            profile= profile_form.save(commit=False)
            profile.user= user

            if 'profile_pic' in request.FILES:
                profile.profile_pic= request.FILES['profile_pic']
            profile.save()

            registered= True
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form= UserForm()
        profile_form= UserProfileInfoForm()

    return render(request, 'QA_interface_app/registration.html',{'user_form': user_form,
                        'profile_form': user_form,
                        'registered': registered,})


def user_login (request):
    if  request.method == 'POST':
        username= request.POST.get('username')
        password= request.POST.get('password')

        user = authenticate( username= username , password= password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))

        else:
            print('someone tried to login and failed!')
            print("Username: {} and password {}" .format(username, password))
            return HttpResponse ( "invalid login details supplied")
    else:
        return render(request, 'QA_interface_app/login.html', {})


@login_required
def  user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


@login_required
def special(request):
    return HttpResponse('you are logged in , nice! ')


@login_required
def report( request):

    #  information in about the machine in the database
    ############################################################
    report_list= Report.objects.order_by('service')#[:5] # to limit the list
    report_dict={'report': report_list}
    qa_machine_list= QA_machine_DB.objects.order_by('QAmachine')
    qa_machine_dict= {'qa_report': qa_machine_list }


    #   information   in  the  Form to transport to the database
    ##############################################################
    # all of this until the next # line is a work around
    Booking= BookingForm(request.POST)

    if request.method == 'POST':
    # TODO change this workaround
    #   WORK AROUND we do here what the form should do .....
        post_request_dict = (dict(request.POST)) # convert queryset
        information_from_form=QA_machine_DB.objects.get(QAmachine=post_request_dict['QAmachine'][0])
        information_from_form.status= post_request_dict['status'][0]
        information_from_form.owner= post_request_dict['owner'][0]
        information_from_form.comments= post_request_dict['comments'][0]
        information_from_form.save()
    #######################
        if Booking.is_valid(): # by update is the form not valid . It does not matter  we just want to post the information .....
            return  render(request, 'QA_interface_app/monitoring.html', context= {'reports':report_list , 'machines': qa_machine_list,'BookingForm':Booking })

        else:
            #print('Form not valid', Booking.errors)
            return render(request, 'QA_interface_app/monitoring.html', context= {'reports':report_list , 'machines': qa_machine_list,'BookingForm':Booking })

    return render(request, 'QA_interface_app/monitoring.html', context= {'reports':report_list , 'machines': qa_machine_list,'BookingForm':Booking })
