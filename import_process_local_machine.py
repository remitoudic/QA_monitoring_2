import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','QA_monitoring.settings')
# for django
import django
import pandas as pd
django.setup()
# for data processing
from QA_interface_app.models import QA_machine_DB, Report
import __ssh_python_tranfer as QA_machine
import __get_jira_issues as jira_data
import pandas as pd
# for salt
from subprocess import Popen, PIPE
import re, schedule,  time

###
#
# The class import_process is responsible for :
#  1) find the actual names of the Qa machines working  via salt,
#  2) create for each name a QA_machine_ object in which all the data of the machine is stored
#  3) save in each QA_machine_ object in the databank
#  4) make a queue of small  package and process it  ,  a package of the queue  deals with   4 object and  the wait 2 min
#
# The class  QA_machine_ store all the data of the qa machine
#
###

class QA_machine_:

    def __init__(self, name):
        self.name= name
        self.report= None #  report correspond to  the information collected by the script of david
        self.status= None
        self.qa_machine_validation = False

    def fill_data(self):
        self.report= QA_machine.transfer_data(self.name)

    def validation(self):
        # check that there is some data.  information are collected in a Dataframe
        if isinstance(self.report,   pd.DataFrame):
            self.qa_machine_validation= True

    def find_state_QA_nmachine( self):
    # get status  of the QA machine

        QA = self.report  #  pd dataframe

        JIRA= jira_data.DATA # pd dataframe
        JOIN_QA_JIRA = pd.merge(QA.tail(), JIRA, left_on='issues_key', right_on='issues_key').empty  # to find if intersection between QA and JIRA

        #print( '################## QA_jira',JIRA)

        #################################################################
        #####################  Logic to find if  QA machine is available
        print( ' ########## DEBUG ###########')
        for line in  range(QA.shape[0]):

            if  not (re.search('BOSS-([0-9]*)', QA.branch[line])):
                if QA.branch[0]== 'master' :    # true =>  the join is empty
                    self.status= 'free'

                else :
                    self.status= 'busy'
                    break
            else:
                if JOIN_QA_JIRA:   #  ask  if JOIN_QA_JIRA empty is (l50)
                    self.status= 'free'
                else:
                    self.status= 'busy'
                    break


    def import_data_django(self):
    #         check my stackoverflow   post  for get_or_create and this beautiful  stackoverflow  post for  update_or_create
    #            https://stackoverflow.com/questions/31418714/update-or-create-wont-respect-unique-key
    #            much better exaplain as in the  Django docu

        remote_data= self.report.to_dict('index')
        #print(remote_data )
        #print('##############################',self.name)
        current_machine, _ = QA_machine_DB.objects.update_or_create( QAmachine =self.name,
                            defaults={'status': self.status})

        print (self.name, ' machine creation/update  passed  ! :')

        for key in remote_data.keys():

            Report.objects.update_or_create(service= remote_data[key]['service_name'],
                                                 user=remote_data[key]['user'],
                                                 store= remote_data[key]['shop'],
                                                 deployement_date=remote_data[key]['date_release'],
                                                 dump_date=remote_data[key]['dump_date'],
                                                 branch = remote_data[key]['branch'],
                                                 FK_report= current_machine)
        print ( self.name, '-',self.status, 'in DB :)' , )

class import_process: # Importing process
    def __init__(self):
        self.List_current_machine=None # data from salt
        self.Queue= None
        self.machines_not_validated=[]

    def clean_report_table(self):
         Report.objects.all().delete()

    def get_actual_machine_with_salt(self):
        # http://docs.saltstack.com/en/pdf/Salt-2018.3.0.pdf  page : 349 - 350
        #  just one minion is used  to get acess to data
        process = Popen(['ssh', 'qa0668', 'sudo', 'salt-call mine.get' ,'*' ,'qa.ip_addr',' --out=string '], stdout=PIPE,  stderr= PIPE) # use a qa machine to get the mine , not ask the master
        # similar to bash command : sudo salt-call mine.get '*' 'qa.ip_addr' --out=json
        #result = os.popen("sudo salt-call mine.get '*' 'qa.ip_addr' ").read().split('\n') # if on qa machine
        stdout = process.communicate()[0]
        result= stdout.decode("utf-8").split('\n')
        list_machine= []
        for elt in result:
            if 'qa' in elt:
                # parse the name of  the machine.
                list_machine.append(re.findall('qa[0-9]*', elt)[0])
        self.List_current_machine=list_machine

    def make_machines_queue(self,length_queue_package=4): # 4 is the max observed to get through jump ssh  security restriction
        # build a queue
        machine_packs_with_remainder , last_sublist= [],[]
        counter= 0

        for sublist in range(len(self.List_current_machine)//length_queue_package):
            sublist_=[]
            for elt in range (length_queue_package):
                sublist_.append(self.List_current_machine[counter])
                counter = counter +1
            machine_packs_with_remainder.append(sublist_)
        remainder= len(self.List_current_machine)% length_queue_package


        for elt in range(remainder):
            last_sublist.append(self.List_current_machine[-elt])

        machine_packs_with_remainder.append(last_sublist)

        self.Queue=machine_packs_with_remainder

    def  process_queue(self):
        def fill_and_import_data_sublist(sublist_machine_names):
            for elt in sublist_machine_names:
                print("\n" ,elt)
                machine= QA_machine_(elt)
                machine.fill_data()
                machine.validation()
                if machine.qa_machine_validation == True :
                    machine.find_state_QA_nmachine()
                    #print(machine.name)
                    machine.import_data_django()
                else:
                    self.machines_not_validated.append(machine.name)
                    print ('################### No Data  for' , machine.name, '#############################' )

        counter = 0
        for package in self.Queue :
            counter= counter+1
            print( '########   Iteration:' ,counter)
            fill_and_import_data_sublist(package) # create machine obj fill it send to django db
            print("I'm waiting..." )
            time.sleep(120)



#if __name__ == '__main__':
def main():
    #import process
    my_process= import_process()
    my_process.clean_report_table()
    my_process.get_actual_machine_with_salt()
    my_process.make_machines_queue() # 4 is the max observed for sublist
    print ( '#######################  QA machines queue to process : ####################################### ' )
    print( [item for sublist in my_process.Queue for item in sublist])  # list comprehension to flattern the queue
    print ( '#######################  QA machines queue is processing ####################################### ' )
    my_process.process_queue()
    print ( '#######################   Process Report  ####################################### ' )
    print ("the number of qa machine processed is : " , len( [item for sublist in my_process.Queue for item in sublist]))
    print ('the number of qa machine with no data is : ' , len( my_process.machines_not_validated) )

#####################

    #Machines_queue=[['qa0001', 'qa0002', 'qa0007', 'qa0666', 'qa0668'], ['qa0669', 'qa0670', 'qa0815', 'qa1001', 'qa1010'],['qa1310', 'qa1313', 'qa1337', 'qa1703', 'qa7003'], ['qa0001', 'qa9999', 'qa9876']]
    #machines_=  ['qa1012', 'qa1013', 'qa1139', 'qa1212', 'qa1310',]

#https://amber-md.github.io/pytraj/latest/tutorials/remote_jupyter_notebook

'''
import schedule
import time
import import_process_data_to_server as job_to_do
#https://media.readthedocs.org/pdf/schedule/stable/schedule.pdf


def job():
    job_to_do
    print("I'm working...")

schedule.every(2).minutes.do(job)
#schedule.every(10).seconds.do(job)git
#schedule.every().hour.do(job)
#schedule.every().day.at("10:30").do(job)
#schedule.every().monday.do(job)
#schedule.every().wednesday.at("13:15").do(job)

while True:
    schedule.run_pending()
    #time.sleep(1)
'''
#####
# remi.toudic@jump-qa
# sudo cat /etc/hosts
#  then with the list of the ip connect directly to  the machine.
#############
