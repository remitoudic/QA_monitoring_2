
##### STEP 1: setup Project and App
    - enter command: django-admin startproject PD_project
    - go to inside this folder
    - enter command: django-admin startapp newsletter_app
    - create at the same level a folder 'templates'
    - inside it  create folder with same name as the app created.



##### STEP 2: Project Setting
The purpose of this step 2 and 3 is  to keep  for each app all tools separated.  

 - Go inside the setting project file.
 - add under  'BASE_DIR=...' the line: TEMPLATE_DIR= os.path.join(BASE_DIR,'template')
 - add in INSTALLED_APPS list the name a the app , in our case 'newsletter_app'
 - add in TEMPLATES dictonary  at the key DIR 'TEMPLATE_DIR' in the list.
  It should look like : 'DIRS': [TEMPLATE_DIR].

##### STEP3 : create the html template
- Go to templates folder
- create  inside a folder with the name of your app.
- create inside you html file ( first without template tagging)

##### STEP4: create views
- go to yourapp in the views.py file
- add views like this one

```python
def index(request):
    retunrn render(request, 'newsletter_app/index.html')
```

##### STEP 5:  URLS mapping
URL mapping is a tricky  part :(

  1) For the Project:
  - go to the project url file
  - add at the beginning of the file :
      - from my_new_app import views
      - from django.conf.urls import url, include
  - add also in the urlpatterns list :
      - url(r'^$', views.index, name='index'),
      - url(r'^new_app/', include('new_app.urls'))

    2) For the new app :
    - create in the new_app folder  a  urls.py file
    - inside add

    ```python
    from django.conf.urls import url
    from newsletter_app import views

    #  for template Tagging later
    app_name= 'newsletter_app'

    urlpatterns=[
        url(r'^relative/$', views.relative, name='relative'),
        url(r'^newsletter/$', views.newsletter, name='newsletter'),
        url(r'^other/$', views.other, name='other'),
        url(r'^$', views.index, name='index')
    ```



#####  Python  Virtual environment ( how to create and activate)
to use this tool in order to not mess with your own local python Environment.

1) First Create one "virtualenv"
- If  not installed with your local machine Python :pip install virtualenv.
- go inside the folder created  
- Enter the command virtual

2) Activate /desactivate:
-  For Unix  machine enter the following bash command : source yourVirtualEnvironment/bin/activate
- to exist the "virtualenv" : just enter "deactivate"



####' ' For get an overview of the current state of the project:

- Go to http://127.0.0.1:8000/admin/  
- login:  user: contorion , password: contorion  


####  Data base relational model
 why : to design properly a database  with optimal dependency and reduce  information redundancy


 #  Vocabulary / Definition :
- table: set of tuple with  the same attributes.
- relation:  same as table
- 'Atomic value' = Can not be  decomposed in smaller value ( Codd )
 - Candidate  key:  smallest ( in termes in Attributes ) primary key . in others words not posible to  fronm  this set of attribute a
 primary key.
- "entity type" : Generally, each table/relation represents one "entity type"
- 'non-prime attribute':  is an attribute that is not a part of any candidate key of the relation

- 1NF : Table with just atomic values
- 2NF : in 1NF and  Non-prime attributes of the relation are dependent on the whole of every candidate key.
- 3NF :


#### Production  server
https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-apps-using-gunicorn-http-server-behind-nginx


http://rahmonov.me/posts/run-a-django-app-with-nginx-and-gunicorn/
https://medium.com/techkylabs/django-deployment-on-linux-ubuntu-16-04-with-postgresql-nginx-ssl-e6504a02f224
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Deployment
https://hackernoon.com/deploy-django-app-with-nginx-gunicorn-postgresql-supervisor-9c6d556a25ac
https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-7.html


https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-apps-using-gunicorn-http-server-behind-nginx

# to install in a qa machine  pip3 and virtualenv
https://gist.github.com/Geoyi/d9fab4f609e9f75941946be45000632b

# to create a local env
python3 -m virtualenv QA_machine_monitoring_env



//  <img src="{% static "images/QA_image.jpeg" %}" alt="Uh Oh, didn't show!">
//alert( 'js running !')

#### deployement
http://rahmonov.me/posts/run-a-django-app-with-gunicorn-in-ubuntu-16-04/

- find process on a port (ex: port 8080)
  - sudo lsof -i :8080
- start gunicorn ( ex: port 8080 en mode background)  :
  - gunicorn --daemon --bind 127.0.0.0:8080 -w 3 QA_monitoring.wsgi

#### When git ignore didn't catch everything :(
- rm -rf mydir
https://realpython.com/django-redirects/
https://simpleisbetterthancomplex.com/article/2017/08/19/how-to-render-django-form-manually.html

##### Documentations
https://medium.com/@richyap13/a-simple-tutorial-on-how-to-document-your-python-project-using-sphinx-and-rinohtype-177c22a15b5b

#### start gunicorn
 gunicorn --daemon --bind localhost:8080 -w 3 QA_monitoring.wsgi
 sudo systemctl status gunicorn

# nginx
sudo service nginx start
sudo service nginx stop
sudo service nginx restart   / better way : sudo systemctl restart nginx   / debug : sudo systemctl status nginx

 #### TUNNEL
ssh -L 8081:localhost:8081 -J jump qa1212

debug:
netstat -tulpn

 run jupyter remote
 jupyter notebook --no-browser --port=8889
 ssh -N -f -L localhost:8888:localhost:8889 remi.toudic@qa1212


nohup  scheduler.py >& /dev/null &
nohup sudo /home/remi.toudic/QA_monitoring_2 scheduler.py >& /dev/null &

  # run python  in backgroung without log output
  nohup  scheduler.py >& /dev/null &
  #this command show the pid of the process or look for it
  ps ax | grep scheduler.py
  #to kill the proces:
  kill < pid>

  #to check if the process is running :  
  ps -u remi.toudic (ex: ps -u remi.toudic)



Basic  of  virtualenv:
# install
pip install virtualenv [--user]

# create an env
virtualenv myenv
virtualenv -p /usr/local/bin/pypy myenv # using the pypy distribution

# use the env
source myenv/bin/activate

# exit the env
deactive # which is usable only after you activate the env
.
