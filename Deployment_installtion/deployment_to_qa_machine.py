import os

## 1
# Create the  upstream  here called  app_servers

with open("/etc/nginx/conf.d/QA_monitoring.conf","w+") as f:
    f.write("upstream app_servers {server localhost:8080;}")

# "w+" more flexible mode :  Opens a file for both writing and reading. Overwrites the existing file
#if the file exists. If the file does not exist, creates a new file for reading and writing.

## 2
# create sites-available file
with open("/etc/nginx/sites-available/QA_monitoring","w+") as g:
    g.write(
    """
       # Configuration for Nginx
        server {

            # Running port
            listen 8081;

            # Settings to serve static files
            location ^~ /static/  {

                # Example:
                # root /full/path/to/application/static/file/dir;
                root /QA_monitoring/static/;

            }

            # Serve a static file (ex. favico)
            # outside /static directory
            location = /favico.ico  {

                root /app/favico.ico;

            }

            # Proxy connections to the application servers
            # app_servers
            location / {

                proxy_pass         http://app_servers;
                proxy_redirect     off;
                proxy_set_header   Host $host;
                proxy_set_header   X-Real-IP $remote_addr;
                proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Host $server_name;
            }

      }

    """)


##  3 create symbolic link in sites-enable
# #sudo ln -s /etc/nginx/sites-available/QA_monitoring QA_monitoring

#src= '/etc/nginx/sites-available/QA_monitoring'
#dst= 'etc/nginx/sites-enable/QA_monitoring'
#os.symlink(src,dst)
# https://www.tutorialspoint.com/python/os_symlink.htm
os.system(' sudo ln -s /etc/nginx/sites-available/QA_monitoring QA_monitoring')

# 4 create systemd file

with open('/usr/lib/systemd/system/gunicorn.service','w+') as h:
     h.write(
"""
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=remi.toudic
Group=remi.toudic
WorkingDirectory=/home/remi.toudic/QA_monitoring_2
ExecStart=/home/remi.toudic/QA_machine_monitoring_env/bin/gunicorn --access-logfile - --bind localhost:8080 -w 3 QA_monitoring.wsgi

[Install]
WantedBy=multi-user.target

""")

os.system('sudo systemctl daemon-reload')

#os.system('gunicorn --daemon --bind localhost:8080 -w 3 QA_monitoring.wsgi')

os.system('systemctl daemon-reload')
