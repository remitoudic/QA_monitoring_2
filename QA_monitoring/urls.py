"""QA_monitoring URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))

"""
from django.conf.urls import url, include
from django.contrib import admin
from QA_interface_app import views



urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^logout/', views.user_logout, name= 'logout'),
    url(r'^QA_machines_app/', include('QA_interface_app.urls')),
    url(r'^qa_machine_api$', views.qa_machine_API.as_view()), # must  be shifted  later in QA_interface_app
    url(r'^register/$', views.register ,name = 'register'),
    url(r'^special/', views.special, name='special'),
]
