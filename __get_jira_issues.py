import pandas as pd
import jira.client
from jira.client import JIRA


def pull_data_jira(user, pwd, JQL):
    # connect to jira and get the proejects
    options = {'server': 'https://contorion.atlassian.net'}
    jira = JIRA(options, basic_auth=(user,pwd))

    #get data
    issues= jira.search_issues( opened_sprints_busy_issues , maxResults=15000,)
    statutes_box= dict()
    statutes_box['issues_key'] = list()
    statutes_box['issues_statues'] = list()
    statutes_box['issues_creation'] = list()

    for issue in issues:
        statutes_box['issues_key'].append( str(issue.key) )
        statutes_box['issues_statues'].append (str(issue.fields.status))
        statutes_box['issues_creation'].append (str(issue.fields.created))

    DF= pd.DataFrame(statutes_box)
    print( 'jira tickets')
    print(DF)
    print('++++++++++++++++++')
    return DF

# JQL filter:
busy_2_last_week= 'project = BOSS AND (status = "Ready for QA" ) AND created > 2018-06-06 '
jql_free_QA=  '(status=CLOSED or status=Invalid or status= Merge Conflict or status=Dev Done Progress   or status= Code Review )'
jql_filter_free = 'project = BOSS AND ( status = "In Progress" OR   status = "Code Review" ) AND created > 2018-04-03 '
opened_sprints_busy_issues= 'project = BOSS AND (sprint in openSprints()) AND  (status = "QA In Progress" OR status ="PM review"  OR status ="Ready for QA")'


DATA=pull_data_jira('marvin.robot','ABrainTheSizeOfTheEarth',opened_sprints_busy_issues )
#https://&lt;myurl&gt;/rest/api/2/search?jql=project=&lt;myproject&gt;&amp;status=closed&amp;fields=id,key,status,project&amp;maxResults=5
