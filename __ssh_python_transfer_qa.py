from subprocess import  PIPE , Popen
import ast, pandas, re
#   FOR PULLING FROM A QA_machine
#   here  we  pull and clean data from the qa machine.
#   !!!  the  "__get_data_QA_machine.py" file must be in the same directory. Not as a module
#   because used in a pipe


def pull_data_from_QA_machine( name_QA_machine_ip_adress):
    ' !!!  output = pandas dataframe'
    p1 = open("__get_data_QA_machine.py",'r')
    p2 = Popen(["ssh",'-oStrictHostKeyChecking=no' , name_QA_machine_ip_adress, "python3"], stdin=p1, stdout=PIPE)
    # '-oStrictHostKeyChecking=no' to avoid  msg :'Are you sure you want to continue connecting (yes/no)? Host key verification failed.'

    output = p2.communicate()

    if  len(output[0])> 10:  # because no byte  transfer come out with : b'[{}]\n
        output_python_data= ast.literal_eval(output[0].decode("utf-8"))
        if isinstance( pandas.DataFrame(output_python_data), pandas.DataFrame): # if empty dataframe
            return (name_QA_machine_ip_adress, pandas.DataFrame(output_python_data))
        else:
             return [ name_QA_machine_ip_adress, [] ]
    else:
        return [ name_QA_machine_ip_adress, [] ]


def filter_issue_key(DF_from_pull_data_from_QA_machine):
    ' !!!!   output= Dataframe'
    DF=DF_from_pull_data_from_QA_machine[1]
    name_QA_machine=DF_from_pull_data_from_QA_machine[0]

    #print(DF)
    if isinstance(DF, pandas.DataFrame):
        filter_issue_key=[]
        for elt in list(DF['branch']):
            #print (elt)
            if re.search('BOSS-([0-9]*)', elt):
                filter_issue_key.append(re.findall('BOSS-[0-9]*', elt)[0])
            elif elt == 'master':
                filter_issue_key.append('master')
            else:
                filter_issue_key.append(' not in  BOSS and not master')
        DF['issues_key']= filter_issue_key
        return (name_QA_machine,DF)
    else:
         return(name_QA_machine,[])


def transfer_data (name_QA_machine):
    # equivalent to : cat ssh_get_data_QA_machine.py | ssh qa0668 'python'
    data=pull_data_from_QA_machine(name_QA_machine)
    post_processing= filter_issue_key(data)[1]
    name_QA_machine=filter_issue_key(data)[0] # in case need to return the name
    return(post_processing)

#print('result',transfer_data('10.135.99.237'))

#if __name__ == "__main__":
    #transfer_data('10.135.99.237')
