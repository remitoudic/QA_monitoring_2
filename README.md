# QA Monitoring Interface( still under construction :blush: )


A Web interface through which everybody can see, when,  who deployed , which branch of which service on which QA machine.
Written in python 3.6 using the Django Webframework  ( https://www.djangoproject.com/)


### How to use ?
```
        - 1. build a tunnel in  terminal with the command : ssh -L 8081:localhost:8081 -J jump qa1212
          ( that  switch your localhost to the qa1212 local host )

        - 2. then  on your browser go to: http://127.0.0.1:8081/  

        - 3. and logoin with contorion : contorion  

```
### How to install project ?

####  locally  or ...
```

  0. Requirements:  Python 3 (with the  pip package already with not minimal python3 )

  1. Build project environment with venv:

  2. Activate project environment:

  3.  install packages  for  the project with pip. In  shell console enter:
      - sudo  pip install -r requirements.txt

  4.  then start server with :  ***python manage.py runserver***  

  5. You are done , check http://127.0.0.1:8081/  

```
#### on a remote (QA machine)
```
  1.

```



### Features :
```

#### on local host:
- admin part ( to manipulate the data per hand )
  1. Go to http://127.0.0.1:8000/admin/  
  2. login:  user: contorion , password: contorion  

#### on qa machine :
- Get from your terminal  data with the command   *** curl localhost:8000/qa_machine_api*** you will get a json back.
```
